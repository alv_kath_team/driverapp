package com.driverapp;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.os.Bundle;

import com.driverapp.fragments.AssignmentFragment;
import com.driverapp.fragments.OrderFragment;
import com.driverapp.fragments.SettingFragment;
import com.driverapp.fragments.ViewFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

public class MainActivity extends AppCompatActivity {

    BottomNavigationView bottomMenu;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        context = this;
        bottomMenu = findViewById(R.id.bottom_menu);
        bottomMenu.setOnNavigationItemSelectedListener(item -> {
            switch (item.getItemId()){
                case R.id.navigation_assignment:
                    openFragment(AssignmentFragment.newInstance());
                    return true;
                case R.id.navigation_order:
                    openFragment(OrderFragment.newInstance());
                    return true;
                case R.id.navigation_view:
                    openFragment(ViewFragment.newInstance());
                    return true;
                case R.id.navigation_setting:
                    openFragment(SettingFragment.newInstance());
                    return true;
            }
            return false;
        });
        openFragment(AssignmentFragment.newInstance());
    }

    public void openFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.container, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }
}