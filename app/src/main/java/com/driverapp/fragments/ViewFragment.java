package com.driverapp.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.driverapp.R;


public class ViewFragment extends Fragment {


    public ViewFragment() {
        // Required empty public constructor
    }


    public static ViewFragment newInstance() {
        return new ViewFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_view, container, false);
        return view;
    }
}