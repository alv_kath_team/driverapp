package com.driverapp.activities;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;

import com.driverapp.MainActivity;
import com.driverapp.R;

public class LoginActivity extends AppCompatActivity {

    private ProgressBar mLoading;
    private EditText mNameuser;
    private EditText mPassword;
    private Button mLogin;
    private Context ctx;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);

        ctx = this;
        this.mNameuser = findViewById(R.id.username);
        this.mPassword = findViewById(R.id.password);
        this.mLoading = findViewById(R.id.loading);
        this.mLogin = findViewById(R.id.login);

        this.mLogin.setOnClickListener(v -> {
            Intent intent = new Intent(ctx, MainActivity.class);
            startActivity(intent);
        });
    }
}
